program test_precond
  use yaml_output
  use f_unittests
  use wrapper_MPI
  use time_profiling

  implicit none

  type(mpi_environment) :: env
  
  call mpiinit()

  env = mpi_environment_comm()  

  if (env%iproc == 0) then
     call f_lib_initialize()
     call f_timing_category_group('Convolutions', 'for the quartic ones')

     call yaml_sequence_open("diagonal preconditioning")
     call run(periodicBC_nohybrid_nocong)
     call run(periodicBC_hybrid_nocong)
     call run(surfaceBC_nocong)
     call run(wireBC_nocong)
     call run(freeBC_nocong)
     call yaml_sequence_close()

     call yaml_sequence_open("conjugate gradient preconditioning")
     call run(periodicBC_nohybrid_cong)
     call run(periodicBC_hybrid_cong)
     call run(surfaceBC_cong)
     call run(wireBC_cong)
     call run(freeBC_cong)
     call run(freeBC_lin)
     call yaml_sequence_close()

     call yaml_sequence_open("tail preconditioner")
     call run(tail)
     call yaml_sequence_close()

     call f_lib_finalize()
  end if

  call release_mpi_environment(env)
  call mpifinalize()

contains

  subroutine check_nocong(lr, confdata, ncplx, psi)
    use liborbs_precisions
    use locregs
    use locreg_operations
    use at_domain
    implicit none
    type(locreg_descriptors), intent(in) :: lr
    type(confpot_data), intent(in) :: confdata
    integer, intent(in) :: ncplx
    real(wp), dimension(:), intent(in) :: psi

    type(workarrays_quartic_convolutions) :: lin_w
    type(workarr_precond) :: w
    real(wp) :: scpr, ref

    call allocate_work_arrays(lr, ncplx, w)
    call precondition_ket(0, confdata, ncplx, (/ 0._gp, 0._gp, 0._gp /), &
         lr, -0.5_gp, -0.4_gp, psi, scpr, w, lin_w)
    call compare(scpr, 1._wp, "norm", tol = 2.d-5)
    select case(domain_geocode(lr%mesh_coarse%dom))
    case('F')
       ref = 0.31317265702_wp
    case('S')
       ref = 1.44428042056_wp
    case('P')
       ref = 2.40111582100_wp
    case('W')
       ref = 0.99998226983_wp
    end select
    call compare(sum(psi**2), ref, "after preconditioning", tol = 1d-11)
    call deallocate_work_arrays(lr%mesh, lr%hybrid_on, ncplx, w)
  end subroutine check_nocong

  subroutine check_cong(lr, confdata, ncplx, psi)
    use f_precisions, only: f_long
    use liborbs_precisions
    use locregs
    use locreg_operations
    use dynamic_memory
    use at_domain
    implicit none
    type(locreg_descriptors), intent(in) :: lr
    type(confpot_data), intent(in) :: confdata
    integer, intent(in) :: ncplx
    real(wp), dimension(:), intent(in) :: psi

    type(workarrays_quartic_convolutions) :: lin_w
    type(workarr_precond) :: w
    real(wp) :: scpr, ref
    real(wp), dimension(:), allocatable :: psi_
    integer(f_long) :: memwork

    if (ncplx > 1) then
       psi_ = f_malloc(psi, id = "psi")
       call allocate_work_arrays(lr, ncplx, w)
       call memspace_work_arrays_precond(lr, ncplx, memwork)
       call precondition_ket(6, confdata, ncplx, (/ 0.1_gp, 0.2_gp, 0.3_gp /), &
            lr, -0.5_gp, -0.4_gp, psi_, scpr, w, lin_w)
       call deallocate_work_arrays(lr%mesh, lr%hybrid_on, ncplx, w)
       call compare(scpr, 1._wp, "norm", tol = 2.d-5)
       select case(domain_geocode(lr%mesh_coarse%dom))
       case('W')
          call compare(memwork, int(602112, f_long), "memory at k")
          ref = 0.71707162380_wp
       case('S')
          call compare(memwork, int(430080, f_long), "memory at k")
          ref = 1.70765972588_wp
       case('P')
          call compare(memwork, int(322560, f_long), "memory at k")
          ref = 2.93117422154_wp
       end select
       call compare(sum(psi_**2), ref, "at k non null", tol = 1d-11)
       call f_free(psi_)
    end if
    psi_ = f_malloc(psi(1:array_dim(lr)), id = "psi")
    psi_ = psi_ * sqrt(real(ncplx))
    call allocate_work_arrays(lr, 1, w)
    call memspace_work_arrays_precond(lr, 1, memwork)
    if (confdata%potorder > 0) then
       call init_local_work_arrays(lr, .true., lin_w)
    end if
    call precondition_ket(6, confdata, 1, (/ 0._gp, 0._gp, 0._gp /), &
         lr, -0.5_gp, -0.4_gp, psi_, scpr, w, lin_w)
    if (confdata%potorder > 0) then
       call deallocate_workarrays_quartic_convolutions(lin_w)
    end if
    call deallocate_work_arrays(lr%mesh, lr%hybrid_on, 1, w)
    call compare(scpr, 1._wp, "norm", tol = 2.d-5)
    select case(domain_geocode(lr%mesh_coarse%dom))
    case('F')
       call compare(memwork, int(26275, f_long), "memory at gamma")
       if (confdata%potorder > 0) then
          ref = 0.00662275799_wp
       else
          ref = 0.35351039090_wp
       end if
    case('W')
       call compare(memwork, int(301482, f_long), "memory at gamma")
       ref = 0.74266879823_wp
    case('S')
       call compare(memwork, int(215485, f_long), "memory at gamma")
       ref = 1.88856151187_wp
    case('P')
       if (ncplx == 2) then
          call compare(memwork, int(161777, f_long), "memory at gamma")
          ref = 3.80223191349_wp
       else
          ! hybrid case
          call compare(memwork, int(63870, f_long), "memory at gamma")
          ref = 3.80223204363_wp
       end if
    end select
    call compare(sum(psi_**2), ref, "at gamma", tol = 1d-11)
    call f_free(psi_)
  end subroutine check_cong

  subroutine periodicBC_nohybrid_nocong(label)
    use liborbs_precisions
    use setup
    implicit none
    character(len = *), intent(out) :: label

    type(setup_data) :: data
    integer, parameter :: ncplx = 2

    label = "Periodic BC, no hybrid"

    call setup_periodicBC(data, .false., ndims = (/ 20, 24, 21 /))
    call setup_psi(data, ncplx)
    call setup_confdata(data, 0._gp, 0)
    call check_nocong(data%lr, data%confdata, ncplx, data%psi)
    call setup_clean(data)
  end subroutine periodicBC_nohybrid_nocong

  subroutine periodicBC_nohybrid_cong(label)
    use liborbs_precisions
    use setup
    implicit none
    character(len = *), intent(out) :: label

    type(setup_data) :: data
    integer, parameter :: ncplx = 2

    label = "Periodic BC, no hybrid"

    call setup_periodicBC(data, .false., ndims = (/ 20, 24, 21 /))
    call setup_psi(data, ncplx)
    call setup_confdata(data, 0._gp, 0)
    call check_cong(data%lr, data%confdata, ncplx, data%psi)
    call setup_clean(data)
  end subroutine periodicBC_nohybrid_cong

  subroutine periodicBC_hybrid_nocong(label)
    use liborbs_precisions
    use setup
    implicit none
    character(len = *), intent(out) :: label

    type(setup_data) :: data
    integer, parameter :: ncplx = 2

    label = "Periodic BC, hybrid"

    call setup_periodicBC(data, .true., ndims = (/ 20, 24, 21 /))
    call setup_psi(data, ncplx)
    call setup_confdata(data, 0._gp, 0)
    call check_nocong(data%lr, data%confdata, ncplx, data%psi)
    call setup_clean(data)
  end subroutine periodicBC_hybrid_nocong

  subroutine periodicBC_hybrid_cong(label)
    use liborbs_precisions
    use setup
    implicit none
    character(len = *), intent(out) :: label

    type(setup_data) :: data
    integer, parameter :: ncplx = 1

    label = "Periodic BC, hybrid"

    call setup_periodicBC(data, .true., ndims = (/ 20, 24, 21 /))
    call setup_psi(data, ncplx)
    call setup_confdata(data, 0._gp, 0)
    call check_cong(data%lr, data%confdata, ncplx, data%psi)
    call setup_clean(data)
  end subroutine periodicBC_hybrid_cong

  subroutine surfaceBC_nocong(label)
    use liborbs_precisions
    use setup
    implicit none
    character(len = *), intent(out) :: label

    type(setup_data) :: data
    integer, parameter :: ncplx = 2

    label = "Surface BC"

    call setup_surfaceBC(data, ndims = (/ 20, 24, 21 /))
    call setup_psi(data, ncplx)
    call setup_confdata(data, 0._gp, 0)
    call check_nocong(data%lr, data%confdata, ncplx, data%psi)
    call setup_clean(data)
  end subroutine surfaceBC_nocong

  subroutine surfaceBC_cong(label)
    use liborbs_precisions
    use setup
    implicit none
    character(len = *), intent(out) :: label

    type(setup_data) :: data
    integer, parameter :: ncplx = 2

    label = "Surface BC"

    call setup_surfaceBC(data, ndims = (/ 20, 24, 21 /))
    call setup_psi(data, ncplx)
    call setup_confdata(data, 0._gp, 0)
    call check_cong(data%lr, data%confdata, ncplx, data%psi)
    call setup_clean(data)
  end subroutine surfaceBC_cong

  subroutine wireBC_nocong(label)
    use liborbs_precisions
    use setup
    implicit none
    character(len = *), intent(out) :: label

    type(setup_data) :: data
    integer, parameter :: ncplx = 2

    label = "Wire BC"

    call setup_wireBC(data, ndims = (/ 20, 24, 21 /))
    call setup_psi(data, ncplx)
    call setup_confdata(data, 0._gp, 0)
    call check_nocong(data%lr, data%confdata, ncplx, data%psi)
    call setup_clean(data)
  end subroutine wireBC_nocong

  subroutine wireBC_cong(label)
    use liborbs_precisions
    use setup
    implicit none
    character(len = *), intent(out) :: label

    type(setup_data) :: data
    integer, parameter :: ncplx = 2

    label = "Wire BC"

    call setup_wireBC(data, ndims = (/ 20, 24, 21 /))
    call setup_psi(data, ncplx)
    call setup_confdata(data, 0._gp, 0)
    call check_cong(data%lr, data%confdata, ncplx, data%psi)
    call setup_clean(data)
  end subroutine wireBC_cong

  subroutine freeBC_nocong(label)
    use liborbs_precisions
    use setup
    implicit none
    character(len = *), intent(out) :: label

    type(setup_data) :: data
    integer, parameter :: ncplx = 2

    label = "Free BC"

    call setup_freeBC(data)
    call setup_psi(data, ncplx)
    call setup_confdata(data, 0._gp, 0)
    call check_nocong(data%lr, data%confdata, ncplx, data%psi)
    call setup_clean(data)
  end subroutine freeBC_nocong

  subroutine freeBC_cong(label)
    use liborbs_precisions
    use setup
    implicit none
    character(len = *), intent(out) :: label

    type(setup_data) :: data
    integer, parameter :: ncplx = 1

    label = "Free BC, without confinment"

    call setup_freeBC(data)
    call setup_psi(data, ncplx)
    call setup_confdata(data, 0._gp, 0)
    call check_cong(data%lr, data%confdata, ncplx, data%psi)
    call setup_clean(data)
  end subroutine freeBC_cong

  subroutine freeBC_lin(label)
    use liborbs_precisions
    use setup
    implicit none
    character(len = *), intent(out) :: label

    type(setup_data) :: data
    integer, parameter :: ncplx = 1

    label = "Free BC, with confinment"

    call setup_freeBC(data)
    call setup_psi(data, ncplx)
    call setup_confdata(data, 2._gp, 4)
    call check_cong(data%lr, data%confdata, ncplx, data%psi)
    call setup_clean(data)
  end subroutine freeBC_lin

  subroutine tail(label)
    use liborbs_precisions
    use setup
    implicit none
    character(len = *), intent(out) :: label

    type(setup_data) :: data
    integer, parameter :: ncplx = 1

    label = "Free BC"

    call setup_freeBC(data)
    call setup_psi(data, ncplx)

    call precong(data%lr, 6, -0.5_gp, data%psi)
    call compare(sum(data%psi**2), 7820.114763483914_gp, "after preconditioning", tol = 1d-11)

    call setup_clean(data)
  end subroutine tail

end program test_precond
