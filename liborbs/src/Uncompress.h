#ifndef UNCOMPRESS_H
#define UNCOMPRESS_H

#include "liborbs_ocl.h"

void create_uncompress_kernels(liborbs_context * context, struct liborbs_kernels * kernels);
void clean_uncompress_kernels(struct liborbs_kernels * kernels);

void build_uncompress_programs(liborbs_context * context);
void clean_uncompress_programs(liborbs_context * context);


/** Scales the wavefunction in compressed form.
 *  @param command_queue used to process the data.
 *  @param nvctr_c number of point of coarse data.
 *  @param nvctr_f number of point of fine data.
 *  @param h hgrid of the system, vector of 3 values.
 *  @param c constant part of the scaling.
 *  @param psi_c array of size nvctr_c * sizeof(double), containing coarse input and output data.
 *  @param psi_f array of size nvctr_f * 7 * sizeof(double), containing fine input and output data.
 */
void scale_psi_d(liborbs_command_queue *command_queue,
                 cl_uint nvctr_c, cl_uint nvctr_f, const double h[3], double c,
                 cl_mem psi_c,  cl_mem psi_f);

/** Uncompresses a wave function using BigDFT sparse wave function compression.
 *  @param command_queue used to process the data.
 *  @param dimensions of the output data, vector of 3 values.
 *  @param nseg_c number of segment of coarse data.
 *  @param nvctr_c number of point of coarse data.
 *  @param keyg_c array of size 2 * nseg_c * sizeof(uint), representing the beginning and end of coarse segments in the output data.
 *  @param keyv_c array of size nseg_c * sizeof(uint), representing the beginning of coarse segments in the input data.
 *  @param nseg_f number of segment of fine data.
 *  @param nvctr_f number of point of fine data.
 *  @param keyg_f array of size 2 * nseg_f * sizeof(uint), representing the beginning and end of fine segments in the output data.
 *  @param keyv_f array of size nseg_f * sizeof(uint), representing the beginning of fine segments in the input data.
 *  @param psi_c array of size nvctr_c * sizeof(double), containing coarse input data.
 *  @param psi_f array of size nvctr_f * 7 * sizeof(double), containing fine input data.
 *  @param psi_out array of size (2 * dimensions[0]) * (2 * dimensions[1]) * (2 * dimensions[2]) containing output data.
 */
void uncompress_d(liborbs_command_queue *command_queue, const cl_uint dimensions[3],
                  cl_uint nseg_c, cl_uint nvctr_c, cl_mem keyg_c, cl_mem keyv_c, 
                  cl_uint nseg_f, cl_uint nvctr_f, cl_mem keyg_f, cl_mem keyv_f,
                  cl_mem psi_c, cl_mem psi_f, cl_mem psi_out);

/** Compresses a wave function using BigDFT sparse wave function compression.
 *  @param command_queue used to process the data.
 *  @param dimensions of the input data, vector of 3 values.
 *  @param nseg_c number of segment of coarse data.
 *  @param nvctr_c number of point of coarse data.
 *  @param keyg_c array of size 2 * nseg_c * sizeof(uint), representing the beginning and end of coarse segments in the input data.
 *  @param keyv_c array of size nseg_c * sizeof(uint), representing the beginning of coarse segments in the output data.
 *  @param nseg_f number of segment of fine data.
 *  @param nvctr_f number of point of fine data.
 *  @param keyg_f array of size 2 * nseg_f * sizeof(uint), representing the beginning and end of fine segments in the input data.
 *  @param keyv_f array of size nseg_f * sizeof(uint), representing the beginning of fine segments in the output data.
 *  @param psi_c array of size nvctr_c * sizeof(double), containing coarse output data.
 *  @param psi_f array of size nvctr_f * 7 * sizeof(double), containing fine output data.
 *  @param psi_out array of size (2 * dimensions[0]) * (2 * dimensions[1]) * (2 * dimensions[2]) containing input data.
 */
void compress_d(liborbs_command_queue *command_queue, const cl_uint dimensions[3],
                cl_uint nseg_c, cl_uint nvctr_c, cl_mem keyg_c, cl_mem keyv_c, 
                cl_uint nseg_f, cl_uint nvctr_f, cl_mem keyg_f, cl_mem keyv_f,
                cl_mem psi_c, cl_mem psi_f, cl_mem psi);

/** Uncompresses and scales a wave function using BigDFT sparse wave function compression.
 *  @param command_queue used to process the data.
 *  @param dimensions of the output data, vector of 3 values.
 *  @param h hgrid of the system, vector of 3 values.
 *  @param c constant part of the scaling.
 *  @param nseg_c number of segment of coarse data.
 *  @param nvctr_c number of point of coarse data.
 *  @param keyg_c array of size 2 * nseg_c * sizeof(uint), representing the beginning and end of coarse segments in the output data.
 *  @param keyv_c array of size nseg_c * sizeof(uint), representing the beginning of coarse segments in the input data.
 *  @param nseg_f number of segment of fine data.
 *  @param nvctr_f number of point of fine data.
 *  @param keyg_f array of size 2 * nseg_f * sizeof(uint), representing the beginning and end of fine segments in the output data.
 *  @param keyv_f array of size nseg_f * sizeof(uint), representing the beginning of fine segments in the input data.
 *  @param psi_c array of size nvctr_c * sizeof(double), containing coarse input data.
 *  @param psi_f array of size nvctr_f * 7 * sizeof(double), containing fine input data.
 *  @param psi_out array of size (2 * dimensions[0]) * (2 * dimensions[1]) * (2 * dimensions[2]) containing output data.
 */
void uncompress_scale_d(liborbs_command_queue *command_queue, const cl_uint dimensions[3], const double h[3], double c,
                        cl_uint nseg_c, cl_uint nvctr_c, cl_mem keyg_c, cl_mem keyv_c,
                        cl_uint nseg_f, cl_uint nvctr_f, cl_mem keyg_f, cl_mem keyv_f,
                        cl_mem psi_c, cl_mem psi_f, cl_mem psi_out);

/** Compresses and scales a wave function using BigDFT sparse wave function compression.
 *  @param command_queue used to process the data.
 *  @param dimensions of the input data, vector of 3 values.
 *  @param h hgrid of the system, vector of 3 values.
 *  @param c constant part of the scaling.
 *  @param nseg_c number of segment of coarse data.
 *  @param nvctr_c number of point of coarse data.
 *  @param keyg_c array of size 2 * nseg_c * sizeof(uint), representing the beginning and end of coarse segments in the input data.
 *  @param keyv_c array of size nseg_c * sizeof(uint), representing the beginning of coarse segments in the output data.
 *  @param nseg_f number of segment of fine data.
 *  @param nvctr_f number of point of fine data.
 *  @param keyg_f array of size 2 * nseg_f * sizeof(uint), representing the beginning and end of fine segments in the input data.
 *  @param keyv_f array of size nseg_f * sizeof(uint), representing the beginning of fine segments in the output data.
 *  @param psi_c array of size nvctr_c * sizeof(double), containing coarse output data.
 *  @param psi_f array of size nvctr_f * 7 * sizeof(double), containing fine output data.
 *  @param psi_out array of size (2 * dimensions[0]) * (2 * dimensions[1]) * (2 * dimensions[2]) containing input data.
 */
void compress_scale_d(liborbs_command_queue *command_queue, const cl_uint dimensions[3], const double h[3], double c,
                      cl_uint nseg_c, cl_uint nvctr_c, cl_mem keyg_c, cl_mem keyv_c, 
                      cl_uint nseg_f, cl_uint nvctr_f, cl_mem keyg_f, cl_mem keyv_f,
                      cl_mem psi_c, cl_mem psi_f, cl_mem psi);

#endif
