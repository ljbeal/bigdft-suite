module orbitals_base
  implicit none
  
  type, public :: real_space_function
    ! wavelets plus scaling functions coefficients
    real(wp), dimension(:,:,:), pointer :: f_real => null()    !dpsi
    real(wp), dimension(:,:,:), pointer :: f_imag => null()    !ipsi
    real(wp), dimension(:,:,:,:), pointer :: f_cmplx => null() !zpsi
    type(domain) ! hgrid, ndims, starting, bc, angles
  end type real_space_function

  type, public :: orbital
    type(wavelet_function) :: phiw
    type(real_space_function) :: phir
  end type orbital

  type, public :: orbital_storage
    real(wp), dimension(:), pointer :: phis_wavelet
    type(workarrays) :: work
  end type orbital_storage

  type, public :: orbitals
    type(orbital), dimension(:), pointer :: phi !phi_1, phi_2, ...
    type(orbital_storage) :: store
  end type orbitals

end module orbitals_base
