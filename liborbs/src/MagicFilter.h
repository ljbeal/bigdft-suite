#ifndef MAGICFILTER_H
#define MAGICFILTER_H

#include "liborbs_ocl.h"

void create_magicfilter_kernels(liborbs_context * context, struct liborbs_kernels * kernels);
void build_magicfilter_programs(liborbs_context * context);

void clean_magicfilter_kernels(struct liborbs_kernels * kernels);
void clean_magicfilter_programs(liborbs_context * context);

/** Performs the one dimensional reciprocal magicfilter and transposition with open boundary conditions.
 *  @param command_queue used to process the convolution.
 *  @param n size of the dimension to process the convolution.
 *  @param ndat size of the other dimension.
 *  @param psi input buffer of size ndat * (n +15) * sizeof(double), stored in column major order.
 *  @param out output buffer of size n * ndat * sizeof(double), stored in column major order.
 */
void magicfiltershrink1d_d(liborbs_command_queue *command_queue,
                           cl_uint n,cl_uint ndat,cl_mem psi,cl_mem out);

/** Performs the one dimensional magicfilter and transposition with open boundary conditions.
 *  @param command_queue used to process the convolution.
 *  @param n size of the dimension to process the convolution.
 *  @param ndat size of the other dimension.
 *  @param psi input buffer of size ndat * n * sizeof(double), stored in column major order.
 *  @param out output buffer of size (n + 15) * ndat * sizeof(double), stored in column major order.
 */
void magicfiltergrow1d_d(liborbs_command_queue *command_queue,
                         cl_uint n,cl_uint ndat,cl_mem psi,cl_mem out);

/** Performs the one dimensional magicfilter and transposition with periodic boundary conditions.
 *  @param command_queue used to process the convolution.
 *  @param n size of the dimension to process the convolution.
 *  @param ndat size of the other dimension.
 *  @param psi input buffer of size ndat * n * sizeof(double), stored in collumn major order.
 *  @param out output buffer of size n * ndat * sizeof(double), stored in collumn major order.
 */
void magicfilter1d_d(liborbs_command_queue *command_queue,
                     cl_uint n,cl_uint ndat,cl_mem psi,cl_mem out);

/** Performs the one dimensional magicfilter and transposition with periodic boundary conditions. Storage of matrix is changed with respect to magicfilter1d_d.
 *  @param command_queue used to process the convolution.
 *  @param n size of the dimension to process the convolution.
 *  @param ndat size of the other dimension.
 *  @param psi input buffer of size n * ndat * sizeof(double), stored in collumn major order.
 *  @param out output buffer of size ndat * n * sizeof(double), stored in collumn major order.
 */
void magicfilter1d_straight_d(liborbs_command_queue *command_queue,
                              cl_uint n,cl_uint ndat,cl_mem psi,cl_mem out);

/** Slightly more performing version of magicfilter1d_d. @see magicfilter1d_d. */
void magicfilter1d_block_d(liborbs_command_queue *command_queue,
                           cl_uint n,cl_uint ndat,cl_mem psi,cl_mem out);

/** Performs the one dimensional magicfilter and transposition with periodic boundary conditions and multiplies by a potential.
 *  @param command_queue used to process the convolution.
 *  @param n size of the dimension to process the convolution.
 *  @param ndat size of the other dimension.
 *  @param psi input buffer of size ndat * n * sizeof(double), stored in collumn major order.
 *  @param pot potential applied during the convolution. Size is n * ndat * sizeof(double), stored in collumn major order.
 *  @param out output buffer of size n * ndat * sizeof(double), stored in collumn major order.
 */
void magicfilter1d_pot_d(liborbs_command_queue *command_queue,
                         cl_uint n, cl_uint ndat, cl_mem psi, cl_mem pot, cl_mem out);

/** Performs the one dimensional reciprocal magicfilter and transposition with periodic boundary conditions.
 *  @param command_queue used to process the convolution.
 *  @param n size of the dimension to process the convolution.
 *  @param ndat size of the other dimension.
 *  @param psi input buffer of size ndat * n * sizeof(double), stored in collumn major order.
 *  @param out output buffer of size n * ndat * sizeof(double), stored in collumn major order.
 */
void magicfilter1d_t_d(liborbs_command_queue *command_queue,
                       cl_uint n,cl_uint ndat,cl_mem psi,cl_mem out);

/** Version of magicfilter_n_d without the temporary buffer, psi is erased during the computation. @see magicfilter_n_d. */
void magicfilter_n_self_d(liborbs_command_queue *command_queue,
                          const cl_uint dimensions[3], cl_mem psi, cl_mem out);

/** Performs the three-dimensional magicfilter with periodic boundary conditions.
 *  @param command_queue used to process the convolution.
 *  @param dimensions of the input data. Vector of three values, one for each dimension.
 *  @param tmp temporary buffer to store intermediate results. Must be of at least dimensions[0] * dimensions[1] * dimensions[2] * sizeof(double) in size.
 *  @param psi input buffer of dimension : dimensions[0] * dimensions[1] * dimensions[2] * sizeof(double). Stored in column major order.
 *  @param out output buffer of dimensions : dimensions[0] * dimensions[1] * dimensions[2] * sizeof(double). Stored in column major order.
 */
void magicfilter_n_d(liborbs_command_queue *command_queue,
                     const cl_uint dimensions[3], cl_mem tmp, cl_mem psi, cl_mem out);

/** Slightly more performing version of magicfilter_n_d. @see magicfilter_n_d. */
void magicfilter_n_straight_d(liborbs_command_queue *command_queue,
                              const cl_uint dimensions[3], cl_mem tmp, cl_mem psi, cl_mem out);

/** Slightly more performing version of magicfilter_n_d. @see magicfilter_n_d. */
void magicfilter_n_block_d(liborbs_command_queue *command_queue,
                           const cl_uint dimensions[3], cl_mem tmp, cl_mem psi, cl_mem out);

/** Performs the three-dimensional magicfilter with periodic boundary conditions, and squares the values to compute the density.
 *  @param command_queue used to process the convolution.
 *  @param dimensions of the input data. Vector of three values, one for each dimension.
 *  @param tmp temporary buffer to store intermediate results. Must be of at least dimensions[0] * dimensions[1] * dimensions[2] * sizeof(double) in size.
 *  @param psi input buffer of dimension : dimensions[0] * dimensions[1] * dimensions[2] * sizeof(double). Stored in column major order.
 *  @param out output buffer of dimensions : dimensions[0] * dimensions[1] * dimensions[2] * sizeof(double). Stored in column major order.
 */
void magicfilter_den_d(liborbs_command_queue *command_queue,
                       const cl_uint dimensions[3], cl_mem tmp, cl_mem psi, cl_mem out);

/** Performs the three-dimensional magicfilter with periodic boundary conditions.
 *  @param command_queue used to process the convolution.
 *  @param dimensions of the input data. Vector of three values, one for each dimension.
 *  @param tmp temporary buffer to store intermediate results. Must be of at least dimensions[0] * dimensions[1] * dimensions[2] * sizeof(double) in size.
 *  @param psi input buffer of dimension : dimensions[0] * dimensions[1] * dimensions[2] * sizeof(double). Stored in column major order.
 *  @param out output buffer of dimensions : dimensions[0] * dimensions[1] * dimensions[2] * sizeof(double). Stored in column major order.
 */
void magicfilter_den_d_generic(liborbs_command_queue *command_queue,
                               const cl_uint dimensions[3], const cl_uint periodic[3],
                               cl_mem tmp, cl_mem psi, cl_mem out);

/** Version of magicfilter_t_d without the temporary buffer, psi is erased during the computation. @see magicfilter_t_d. */
void magicfilter_t_self_d(liborbs_command_queue *command_queue,
                          const cl_uint dimensions[3], cl_mem psi, cl_mem out);

/** Performs the three-dimensional magicfilter with periodic or non-periodic boundary conditions.
 *  @param command_queue used to process the convolution.
 *  @param dimensions of the input data. Vector of three values, one for each dimension.
 *  @param periodic periodicity of the convolution. Vector of three value, one for each dimension. Non zero means periodic.
 *  @param tmp temporary buffer to store intermediate results. Must be of at least (2 * dimensions[0] + (periodic[0]?0:14+15)) * (2 * dimensions[1] + (periodic[1]?0:14+15)) * (2 * dimensions[2] + (periodic[2]?0:14+15)) * sizeof(double) in size.
 *  @param psi input buffer of dimension : (2 * dimensions[0] + (periodic[0]?0:14+15)) * (2 * dimensions[1] + (periodic[1]?0:14+15)) * (2 * dimensions[2] + (periodic[2]?0:14+15)) * sizeof(double). Stored in column major order.
 *  @param out output buffer of dimensions : (2 * dimensions[0] + (periodic[0]?0:14+15)) * (2 * dimensions[1] + (periodic[1]?0:14+15)) * (2 * dimensions[2] + (periodic[2]?0:14+15)) * sizeof(double). Stored in column major order.
 */
void magicfilter_t_d(liborbs_command_queue *command_queue,
                     const cl_uint dimensions[3], cl_mem tmp, cl_mem psi, cl_mem out);

void magic_filter_3d_generic(liborbs_command_queue *command_queue,
                             const cl_uint dimensions[3], const cl_uint periodic[3],
                             cl_mem tmp, cl_mem tmp_dot, cl_mem psi, cl_mem out);

void magic_filter_t_3d_generic(liborbs_command_queue *command_queue,
                               const cl_uint dimensions[3], const cl_uint periodic[3],
                               cl_mem tmp, cl_mem tmp_dot, cl_mem psi, cl_mem out);

/** Performs the three-dimensional magicfilter, applies the potential then applies the reciprocal three dimension magicfilter. The potential energy is also computed. With periodic or non periodic boundary conditions.
 *  @param command_queue used to process the convolution.
 *  @param dimensions of the input data. Vector of three values, one for each dimension.
 *  @param periodic periodicity of the convolution. Vector of three value, one for each dimension. Non zero means periodic.
 *  @param tmp temporary buffer to store intermediate results. Must be of at least (2 * dimensions[0] + (periodic[0]?0:14+15)) * (2 * dimensions[1] + (periodic[1]?0:14+15)) * (2 * dimensions[2] + (periodic[2]?0:14+15)) * sizeof(double) in size.
 *  @param tmp_dot temporary buffer to store intermediate results. Must be of at least (2 * dimensions[0] + (periodic[0]?0:14)) * (2 * dimensions[1] + (periodic[1]?0:14)) * (2 * dimensions[2] + (periodic[2]?0:14)) * sizeof(double) in size.
 *  @param psi input buffer of dimension : (2 * dimensions[0] + (periodic[0]?0:14)) * (2 * dimensions[1] + (periodic[1]?0:14)) * (2 * dimensions[2] + (periodic[2]?0:14)) * sizeof(double). Stored in column major order.
 *  @param out output buffer of dimensions : (2 * dimensions[0] + (periodic[0]?0:14+15)) * (2 * dimensions[1] + (periodic[1]?0:14+15)) * (2 * dimensions[2] + (periodic[2]?0:14+15)) * sizeof(double). Stored in column major order.
 *  @param pot potential applied, buffer of dimensions : (2 * dimensions[0] + (periodic[0]?0:14+15)) * (2 * dimensions[1] + (periodic[1]?0:14+15)) * (2 * dimensions[2] + (periodic[2]?0:14+15)) * sizeof(double). Stored in column major order.
 *  @param epot potential energy computed.
 */
void potential_application_d_generic(liborbs_command_queue *command_queue,
                                     const cl_uint dimensions[3], const cl_uint periodic[3],
                                     cl_mem tmp, cl_mem tmp_dot,
                                     cl_mem psi, cl_mem out, cl_mem pot, double *epot);

/** Performs the three-dimensional magicfilter, applies the potential then applies the reciprocal three dimension magicfilter. With periodic boundary conditions.
 *  @param command_queue used to process the convolution.
 *  @param dimensions of the input data. Vector of three values, one for each dimension.
 *  @param tmp temporary buffer to store intermediate results. Must be of at least dimensions[0] * dimensions[1] * dimensions[2] * sizeof(double) in size.
 *  @param psi input buffer of dimension : (2 * dimensions[0]) * (2 * dimensions[1]) * (2 * dimensions[2]) * sizeof(double). Stored in column major order.
 *  @param out output buffer of dimensions : (2 * dimensions[0]) * ( 2 * dimensions[1]) * (2 * dimensions[2]) * sizeof(double). Stored in column major order.
 *  @param pot potential applied, buffer of dimensions : (2 * dimensions[0]) * (2 * dimensions[1]) * (2 * dimensions[2]) * sizeof(double). Stored in column major order.
 */
void potential_application_d(liborbs_command_queue *command_queue,
                             const cl_uint dimensions[3], cl_mem tmp, cl_mem psi,
                             cl_mem out, cl_mem pot);

#endif
