Spherical harmonics and multipoles: the :f:mod:`f_harmonics` module
===================================================================

.. f:automodule:: f_harmonics
