#include <Misc>
#include <YamlOutput>
#include <AtDomain>
#include <Iobox>

using namespace Atlab;

int main(int argc, char **ragv)
{
    Domain dom;

    Futile::initialize();

    Futile::Yaml::map("default geocode", dom.geocode());
    
    Futile::FEnumerator units = units_enum_from_str("angstroem");

    {
        int nspin;
        int ndims[3];
        const char cube[] = "test.cube";

        read_field_dimensions(cube, 'F', ndims, nspin);
        Futile::Yaml::mapping_open(cube);
        Futile::Yaml::map("spin components", nspin);
        Futile::Yaml::map("dimensions", ndims, 3);
        Futile::Yaml::mapping_close();

        double hgrids[3];
        const int ldrho = ndims[0] * ndims[1] * ndims[2];
        const int nrho = nspin;
        double *rho = new double[ldrho * nspin];
        read_field(cube, 'F', ndims, hgrids, nspin, ldrho, nrho, rho);
    }

    Futile::finalize();
    
    return 0;
}
