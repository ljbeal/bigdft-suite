#ifndef F_ENUMS_H
#define F_ENUMS_H

#include "futile_cst.h"

namespace Futile {

class FEnumerator
{
public:

  F_DEFINE_TYPE(f_enumerator);
  operator f90_f_enumerator*() { return F_TYPE(mFEnumerator); };
  operator const f90_f_enumerator*() const { return F_TYPE(mFEnumerator); };
  operator f90_f_enumerator_pointer*() { return &mFEnumerator; };
  FEnumerator(f90_f_enumerator_pointer other) : mFEnumerator(other) {};

  FEnumerator(const FEnumerator& other);

  FEnumerator(FEnumerator (*family) = nullptr,
    const int (*id) = nullptr,
    const char (*name)[64] = nullptr);

  ~FEnumerator(void);

  void nullify_f_enum(void);

  void f_enum_attr(FEnumerator& attr);

  void f_enum_update(const FEnumerator& src);

  bool operator ==(const FEnumerator& en1) const;

  bool operator ==(int int_bn) const;

  bool operator ==(const char* char_bn) const;

  bool operator !=(const FEnumerator& en1) const;

  bool operator !=(int int_bn) const;

  bool operator !=(const char* char_bn) const;

  bool enum_has_attribute(const FEnumerator& family) const;

  bool enum_has_char(const char* family) const;

  bool enum_has_int(int family) const;

  FEnumerator enum_get_from_int(int family) const;

  FEnumerator enum_get_from_char(const char* family) const;

  FEnumerator enum_get_from_enum(const FEnumerator& family) const;

  int toi(void) const;

/*   void toa(void) const; */

private:
  f90_f_enumerator_pointer mFEnumerator;

};

bool operator ==(int int_bn,
    const FEnumerator& en);


}
#endif
