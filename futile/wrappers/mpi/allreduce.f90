!> @file
!! Wrapper for mpi_allreduce flavours
!! Use error handling
!! @author
!!    Copyright (C) 2012-2016 BigDFT group
!!    This file is distributed under the terms of the
!!    GNU General Public License, see ~/COPYING file
!!    or http://www.gnu.org/copyleft/gpl.txt .
!!    For the list of contributors, see ~/AUTHORS
module f_allreduce
  use time_profiling, only: TIMING_UNINITIALIZED
  use f_precisions
  use f_enums
  use fmpi_types
  use f_utils, only: f_size, f_zero
  use yaml_strings
  use dictionaries, only: f_err_throw
  use dynamic_memory
  implicit none
  private

  !> Interface for MPI_ALLREDUCE routine, to be updated little by little
  interface fmpi_allreduce
     module procedure mpiallred_int,mpiallred_real
     module procedure mpiallred_double !,&!,mpiallred_double_1,mpiallred_double_2,&
     module procedure mpiallred_log
     module procedure mpiallred_byte
     module procedure mpiallred_long
     module procedure mpiallred_r1,mpiallred_r2,mpiallred_r3,mpiallred_r4
     module procedure mpiallred_d1,mpiallred_d2,mpiallred_d3,mpiallred_d4,mpiallred_d5
     module procedure mpiallred_i1,mpiallred_i2,mpiallred_i3
     module procedure mpiallred_l1,mpiallred_l2,mpiallred_l3
     module procedure mpiallred_b1,mpiallred_b2,mpiallred_b3
     module procedure mpiallred_multi_d0 !,mpiallred_multi_i0
     module procedure mpiallred_irecvbuf 
     module procedure fmpi_allreduce_array_d1, fmpi_allreduce_array_i1
  end interface fmpi_allreduce


  interface fmpi_reduce
     module procedure fmpi_reduce_d0
  end interface fmpi_reduce

  !> Receive datatype for nonblocking receive
  type, public :: fmpi_irecvbuf
    integer :: ncount = 0 !< number of dimension
    real(f_double), dimension(:), pointer :: d_receivebuf => null()
    integer(f_integer), dimension(:), pointer :: i_receivebuf => null()
    integer(fmpi_integer) :: request = FMPI_REQUEST_NULL
  end type fmpi_irecvbuf

  interface assignment(=)
    module procedure fmpi_irecvbuf_new_d, fmpi_irecvbuf_new_i
  end interface

  interface fmpi_irecvbuf_get_data
    module procedure fmpi_irecvbuf_get_data_d0,fmpi_irecvbuf_get_data_d1
    module procedure fmpi_irecvbuf_get_data_i0,fmpi_irecvbuf_get_data_i1      
  end interface

  public :: fmpi_allreduce, assignment(=), fmpi_irecvbuf_get_data, nullify_fmpi_irecvbuf, fmpi_reduce

  logical, parameter :: have_mpi2=.true.

  integer, public, save :: TCAT_ALLRED_SMALL = TIMING_UNINITIALIZED
  integer, public, save :: TCAT_ALLRED_LARGE = TIMING_UNINITIALIZED
  integer, parameter, public :: smallsize=5 !< limit for a communication with small size

  contains

  pure function fmpi_irecvbuf_null() result(w)
    implicit none
    type(fmpi_irecvbuf) :: w
    call nullify_fmpi_irecvbuf(w)
  end function fmpi_irecvbuf_null

  pure subroutine nullify_fmpi_irecvbuf(w)
    implicit none
    type(fmpi_irecvbuf),intent(out) :: w
    w%ncount = 0
    w%request = FMPI_REQUEST_NULL
    nullify(w%d_receivebuf)
    nullify(w%i_receivebuf)
  end subroutine nullify_fmpi_irecvbuf

  subroutine fmpi_irecvbuf_new_d(w, array)
    implicit none
    type(fmpi_irecvbuf), intent(out) :: w
    real(f_double), dimension(:), intent(in) :: array

    call nullify_fmpi_irecvbuf(w)
    w%ncount = size(array)
    w%d_receivebuf = f_malloc_ptr(w%ncount,id='w%d_receivebuf')
    call f_memcpy(src=array,dest=w%d_receivebuf)
  end subroutine fmpi_irecvbuf_new_d

  subroutine fmpi_irecvbuf_wait(w,status)
    use f_sendrecv, only: fmpi_wait
    implicit none
    type(fmpi_irecvbuf), intent(in) :: w
    integer(fmpi_integer), dimension(FMPI_STATUS_SIZE), intent(out), optional :: status

    call fmpi_wait(w%request, status)

  end subroutine fmpi_irecvbuf_wait

  subroutine fmpi_irecvbuf_new_i(w, array)
    implicit none
    type(fmpi_irecvbuf), intent(out) :: w
    integer(f_integer), dimension(:), intent(in) :: array

    call nullify_fmpi_irecvbuf(w)
    w%ncount = size(array)
    w%i_receivebuf = f_malloc_ptr(w%ncount,id='w%i_receivebuf')
    call f_memcpy(src=array,dest=w%i_receivebuf)
  end subroutine fmpi_irecvbuf_new_i

  subroutine fmpi_irecvbuf_get_data_i1(w,array)
    implicit none
    type(fmpi_irecvbuf), intent(inout) :: w
    integer(f_integer), dimension(:), intent(out) :: array

    call fmpi_irecvbuf_wait(w)
    call f_memcpy(src=w%i_receivebuf, dest=array)
    call deallocate_fmpi_irecvbuf(w)
  end subroutine fmpi_irecvbuf_get_data_i1

  subroutine fmpi_irecvbuf_get_data_d1(w,array)
    implicit none
    type(fmpi_irecvbuf), intent(inout) :: w
    real(f_double), dimension(:), intent(out) :: array
    call fmpi_irecvbuf_wait(w)
    call f_memcpy(src=w%d_receivebuf, dest=array)
    call deallocate_fmpi_irecvbuf(w)
  end subroutine fmpi_irecvbuf_get_data_d1

  subroutine fmpi_irecvbuf_get_data_d0(w,val1,val2,&
         val3,val4,val5,val6,val7,val8,val9,val10)
    use f_utils, only: f_assert
    implicit none
    type(fmpi_irecvbuf), intent(inout) :: w
    real(f_double), intent(out) :: val1
    real(f_double), intent(out), optional :: val2,val3,val4,val5,val6,val7,val8,val9,val10
    !local variables
    integer :: count
    real(f_double), dimension(10) :: array

    count=1
    if (present(val2)) count=count+1
    if (present(val3)) count=count+1
    if (present(val4)) count=count+1
    if (present(val5)) count=count+1
    if (present(val6)) count=count+1
    if (present(val7)) count=count+1
    if (present(val8)) count=count+1
    if (present(val9)) count=count+1
    if (present(val10)) count=count+1

    call f_assert(count == w%ncount, id='Invalid count in recvbuf_get_data',&
                  err_id=ERR_MPI_WRAPPERS)

    call fmpi_irecvbuf_wait(w)
    call f_memcpy(src=w%d_receivebuf, dest=array)

    val1 = array(1)
    if (count >= 2) val2 = array(2)
    if (count >= 3) val3 = array(3)
    if (count >= 4) val4 = array(4)
    if (count >= 5) val5 = array(5)
    if (count >= 6) val6 = array(6)
    if (count >= 7) val7 = array(7)
    if (count >= 8) val8 = array(8)
    if (count >= 9) val9 = array(9)
    if (count == 10) val10 = array(10)

    call deallocate_fmpi_irecvbuf(w)
  end subroutine fmpi_irecvbuf_get_data_d0

  subroutine fmpi_irecvbuf_get_data_i0(w,val1,val2,&
         val3,val4,val5,val6,val7,val8,val9,val10)
    use f_utils, only: f_assert
    implicit none
    type(fmpi_irecvbuf), intent(inout) :: w
    integer(f_integer), intent(out) :: val1
    integer(f_integer), intent(out), optional :: val2,val3,val4,val5,val6,val7,val8,val9,val10
    !local variables
    integer :: count
    integer(f_integer), dimension(10) :: array

    count=1
    if (present(val2)) count=count+1
    if (present(val3)) count=count+1
    if (present(val4)) count=count+1
    if (present(val5)) count=count+1
    if (present(val6)) count=count+1
    if (present(val7)) count=count+1
    if (present(val8)) count=count+1
    if (present(val9)) count=count+1
    if (present(val10)) count=count+1

    call f_assert(count == w%ncount, id='Invalid count in recvbuf_get_data',&
                  err_id=ERR_MPI_WRAPPERS)

    call fmpi_irecvbuf_wait(w)
    call f_memcpy(src=w%i_receivebuf, dest=array)

    val1 = array(1)
    if (count >= 2) val2 = array(2)
    if (count >= 3) val3 = array(3)
    if (count >= 4) val4 = array(4)
    if (count >= 5) val5 = array(5)
    if (count >= 6) val6 = array(6)
    if (count >= 7) val7 = array(7)
    if (count >= 8) val8 = array(8)
    if (count >= 9) val9 = array(9)
    if (count == 10) val10 = array(10)

    call deallocate_fmpi_irecvbuf(w)
  end subroutine fmpi_irecvbuf_get_data_i0

  subroutine deallocate_fmpi_irecvbuf(w)
    implicit none
    type(fmpi_irecvbuf),intent(inout) :: w
    call f_free_ptr(w%d_receivebuf)
    call f_free_ptr(w%i_receivebuf)
    call nullify_fmpi_irecvbuf(w)
  end subroutine deallocate_fmpi_irecvbuf

  function fmpi_reduce_d0(sendbuf,op,comm,root) result(recvbuf)
      use f_utils, only: f_get_option
      implicit none
      real(f_double) :: sendbuf
      type(f_enumerator), intent(in), optional :: op
      integer(fmpi_integer), intent(in), optional :: comm
      integer(fmpi_integer), intent(in), optional :: root
      real(f_double) :: recvbuf
      !local variables
      integer(fmpi_integer) :: root_, comm_, ierr

      root_ = f_get_option(opt=root, default=0)
      comm_ = fmpi_comm(comm)
      call MPI_REDUCE(sendbuf,recvbuf,1,mpitype(sendbuf),&
          int(toi(op),fmpi_integer),root_,comm_,ierr)

      if (ierr /= FMPI_SUCCESS) then
       call f_err_throw('An error in calling to MPI_REDUCE occured',&
            err_id=ERR_MPI_WRAPPERS)
       return
      end if

  end function fmpi_reduce_d0


  !>useful for reducing arrays of small size (especially stack-related)
  subroutine fmpi_allreduce_array_d1(val,irecvbuf,op,comm)
    use f_enums
    implicit none
    real(f_double), dimension(:), intent(in) :: val
    type(f_enumerator), intent(in), optional :: op !< operation to be done
    integer(fmpi_integer), intent(in), optional :: comm
    type(fmpi_irecvbuf),intent(out) :: irecvbuf
    !integer, intent(in), optional :: op !< operation to be done
    !local variables
    type(f_enumerator) :: mpi_op

    mpi_op=FMPI_SUM
    if (present(op)) mpi_op=op

    irecvbuf = val
    call fmpi_allreduce(irecvbuf, op=op, comm=comm)
  end subroutine fmpi_allreduce_array_d1

  !>useful for reducing arrays of small size (especially stack-related)
  subroutine fmpi_allreduce_array_i1(val,irecvbuf,op,comm)
    use f_enums
    implicit none
    integer(f_integer), dimension(:), intent(in) :: val
    type(f_enumerator), intent(in), optional :: op !< operation to be done
    type(fmpi_irecvbuf),intent(out) :: irecvbuf
    integer(fmpi_integer), intent(in), optional :: comm
    !integer, intent(in), optional :: op !< operation to be done
    !local variables
    type(f_enumerator) :: mpi_op

    mpi_op=FMPI_SUM
    if (present(op)) mpi_op=op

    irecvbuf = val
    call fmpi_allreduce(irecvbuf, op=op, comm=comm)

  end subroutine fmpi_allreduce_array_i1

    subroutine mpiallred_irecvbuf(irecvbuf,op,comm)
      implicit none
      type(fmpi_irecvbuf), intent(inout) :: irecvbuf
      type(f_enumerator), intent(in) :: op
      integer, intent(in), optional :: comm
      
      if (associated(irecvbuf%d_receivebuf)) then
        call fmpi_allreduce(sendbuf=irecvbuf%d_receivebuf,op=op,comm=comm,request=irecvbuf%request)
      else if (associated(irecvbuf%i_receivebuf)) then
        call fmpi_allreduce(sendbuf=irecvbuf%i_receivebuf,op=op,comm=comm,request=irecvbuf%request)
      end if
    end subroutine mpiallred_irecvbuf

    !> Interface for MPI_ALLREDUCE operations
    subroutine mpiallred_int(sendbuf,count,op,comm,recvbuf,request)
      implicit none
      integer(f_integer) :: sendbuf
      integer(f_integer), intent(inout), optional :: recvbuf
      integer(f_integer), dimension(:), allocatable :: copybuf
      include 'allreduce-inc.f90'
    end subroutine mpiallred_int

    subroutine mpiallred_long(sendbuf,count,op,comm,recvbuf,request)
      implicit none
      integer(f_long) :: sendbuf
      integer(f_long), intent(inout), optional :: recvbuf
      integer(f_long), dimension(:), allocatable :: copybuf
      include 'allreduce-inc.f90'
    end subroutine mpiallred_long

    !> Interface for MPI_ALLREDUCE operations
    subroutine mpiallred_real(sendbuf,count,op,comm,recvbuf,request)
      implicit none
      real(f_simple) :: sendbuf
      real(f_simple), intent(inout), optional :: recvbuf
      real(f_simple), dimension(:), allocatable :: copybuf
      include 'allreduce-inc.f90'
    end subroutine mpiallred_real

    subroutine mpiallred_double(sendbuf,count,op,comm,recvbuf,request)
      implicit none
      real(f_double) :: sendbuf
      real(f_double), intent(inout), optional :: recvbuf
      real(f_double), dimension(:), allocatable :: copybuf
      include 'allreduce-inc.f90'
    end subroutine mpiallred_double

    subroutine mpiallred_log(sendbuf,count,op,comm,recvbuf,request)
      implicit none
      logical :: sendbuf
      logical, intent(inout), optional :: recvbuf
      logical, dimension(:), allocatable :: copybuf
      include 'allreduce-inc.f90'
    end subroutine mpiallred_log

    subroutine mpiallred_byte(sendbuf,count,op,comm,recvbuf,request)
      implicit none
      logical(f_byte) :: sendbuf
      integer(f_integer), intent(inout), optional :: recvbuf
      logical(f_byte), dimension(:), allocatable :: copybuf
      include 'allreduce-inc.f90'
    end subroutine mpiallred_byte

    subroutine mpiallred_i1(sendbuf,op,comm,recvbuf,request)
      implicit none
      integer, dimension(:), intent(inout) :: sendbuf
      integer, dimension(:), intent(inout), optional :: recvbuf
      integer, dimension(:), allocatable :: copybuf
      include 'allreduce-arr-inc.f90'
    end subroutine mpiallred_i1

    subroutine mpiallred_i2(sendbuf,op,comm,recvbuf,request)
      implicit none
      integer, dimension(:,:), intent(inout) :: sendbuf
      integer, dimension(:,:), intent(inout), optional :: recvbuf
      integer, dimension(:,:), allocatable :: copybuf
      include 'allreduce-arr-inc.f90'
    end subroutine mpiallred_i2

    subroutine mpiallred_i3(sendbuf,op,comm,recvbuf,request)
      implicit none
      integer, dimension(:,:,:), intent(inout) :: sendbuf
      integer, dimension(:,:,:), intent(inout), optional :: recvbuf
      integer, dimension(:,:,:), allocatable :: copybuf
      include 'allreduce-arr-inc.f90'
    end subroutine mpiallred_i3


    subroutine mpiallred_l1(sendbuf,op,comm,recvbuf,request)
      implicit none
      logical, dimension(:), intent(inout) :: sendbuf
      logical, dimension(:), intent(inout), optional :: recvbuf
      logical, dimension(:), allocatable :: copybuf
      include 'allreduce-arr-inc.f90'
    end subroutine mpiallred_l1

    subroutine mpiallred_l2(sendbuf,op,comm,recvbuf,request)
      implicit none
      logical, dimension(:,:), intent(inout) :: sendbuf
      logical, dimension(:,:), intent(inout), optional :: recvbuf
      logical, dimension(:,:), allocatable :: copybuf
      include 'allreduce-arr-inc.f90'
    end subroutine mpiallred_l2

    subroutine mpiallred_l3(sendbuf,op,comm,recvbuf,request)
      implicit none
      logical, dimension(:,:,:), intent(inout) :: sendbuf
      logical, dimension(:,:,:), intent(inout), optional :: recvbuf
      logical, dimension(:,:,:), allocatable :: copybuf
      include 'allreduce-arr-inc.f90'
    end subroutine mpiallred_l3

    !subroutine mpiallred_ll3(sendbuf,op,comm,recvbuf,request)
    !  implicit none
    !  logical(f_byte), dimension(:,:,:), intent(inout) :: sendbuf
    !  logical(f_byte), dimension(:,:,:), intent(inout), optional :: recvbuf
    !  logical(f_byte), dimension(:,:,:), allocatable :: copybuf
    !  include 'allreduce-arr-inc.f90'
    !end subroutine mpiallred_ll3

    subroutine mpiallred_b1(sendbuf,op,comm,recvbuf,request)
      implicit none
      logical(f_byte), dimension(:), intent(inout) :: sendbuf
      logical(f_byte), dimension(:), intent(inout), optional :: recvbuf
      logical(f_byte), dimension(:), allocatable :: copybuf
      include 'allreduce-arr-inc.f90'
    end subroutine mpiallred_b1

    subroutine mpiallred_b2(sendbuf,op,comm,recvbuf,request)
      implicit none
      logical(f_byte), dimension(:,:), intent(inout) :: sendbuf
      logical(f_byte), dimension(:,:), intent(inout), optional :: recvbuf
      logical(f_byte), dimension(:,:), allocatable :: copybuf
      include 'allreduce-arr-inc.f90'
    end subroutine mpiallred_b2

    subroutine mpiallred_b3(sendbuf,op,comm,recvbuf,request)
      implicit none
      logical(f_byte), dimension(:,:,:), intent(inout) :: sendbuf
      logical(f_byte), dimension(:,:,:), intent(inout), optional :: recvbuf
      logical(f_byte), dimension(:,:,:), allocatable :: copybuf
      include 'allreduce-arr-inc.f90'
    end subroutine mpiallred_b3


    subroutine mpiallred_r1(sendbuf,op,comm,recvbuf,request)
      implicit none
      real, dimension(:), intent(inout) :: sendbuf
      real, dimension(:), intent(inout), optional :: recvbuf
      real, dimension(:), allocatable :: copybuf
      include 'allreduce-arr-inc.f90'
    end subroutine mpiallred_r1

    subroutine mpiallred_r2(sendbuf,op,comm,recvbuf,request)
      implicit none
      real, dimension(:,:), intent(inout) :: sendbuf
      real, dimension(:,:), intent(inout), optional :: recvbuf
      real, dimension(:,:), allocatable :: copybuf
      include 'allreduce-arr-inc.f90'
    end subroutine mpiallred_r2

    subroutine mpiallred_r3(sendbuf,op,comm,recvbuf,request)
      implicit none
      real, dimension(:,:,:), intent(inout) :: sendbuf
      real, dimension(:,:,:), intent(inout), optional :: recvbuf
      real, dimension(:,:,:), allocatable :: copybuf
      include 'allreduce-arr-inc.f90'
    end subroutine mpiallred_r3

    subroutine mpiallred_r4(sendbuf,op,comm,recvbuf,request)
      implicit none
      real, dimension(:,:,:,:), intent(inout) :: sendbuf
      real, dimension(:,:,:,:), intent(inout), optional :: recvbuf
      real, dimension(:,:,:,:), allocatable :: copybuf
      include 'allreduce-arr-inc.f90'
    end subroutine mpiallred_r4

    subroutine mpiallred_d1(sendbuf,op,comm,recvbuf,request)
      implicit none
      real(f_double), dimension(:), intent(inout) :: sendbuf
      real(f_double), dimension(:), intent(inout), optional :: recvbuf
      real(f_double), dimension(:), allocatable :: copybuf
      include 'allreduce-arr-inc.f90'
    end subroutine mpiallred_d1

    subroutine mpiallred_d2(sendbuf,op,comm,recvbuf,request)
      implicit none
      real(f_double), dimension(:,:), intent(inout) :: sendbuf
      real(f_double), dimension(:,:), intent(inout), optional :: recvbuf
      real(f_double), dimension(:,:), allocatable :: copybuf
      include 'allreduce-arr-inc.f90'
    end subroutine mpiallred_d2

    subroutine mpiallred_d3(sendbuf,op,comm,recvbuf,request)
      implicit none
      real(f_double), dimension(:,:,:), intent(inout) :: sendbuf
      real(f_double), dimension(:,:,:), intent(inout), optional :: recvbuf
      real(f_double), dimension(:,:,:), allocatable :: copybuf
      include 'allreduce-arr-inc.f90'
    end subroutine mpiallred_d3

    subroutine mpiallred_d4(sendbuf,op,comm,recvbuf,request)
      implicit none
      real(f_double), dimension(:,:,:,:), intent(inout) :: sendbuf
      real(f_double), dimension(:,:,:,:), intent(inout), optional :: recvbuf
      real(f_double), dimension(:,:,:,:), allocatable :: copybuf
      include 'allreduce-arr-inc.f90'
    end subroutine mpiallred_d4

    subroutine mpiallred_d5(sendbuf,op,comm,recvbuf,request)
      implicit none
      real(f_double), dimension(:,:,:,:,:), intent(inout) :: sendbuf
      real(f_double), dimension(:,:,:,:,:), intent(inout), optional :: recvbuf
      real(f_double), dimension(:,:,:,:,:), allocatable :: copybuf
      include 'allreduce-arr-inc.f90'
    end subroutine mpiallred_d5

    !>routine for the calling of multiple scalars in one single call
    subroutine mpiallred_multi_d0(val1,val2,&
         val3,val4,val5,val6,val7,val8,val9,val10,op,comm,irecvbuf)
      implicit none
      real(f_double) :: val1,val2
      real(f_double), optional :: val3,val4,val5,val6,val7,val8,val9,val10
      !local variables
      real(f_double), dimension(10) :: tmpsend,tmprecv

      include 'allreduce-multi-inc.f90'

    end subroutine mpiallred_multi_d0

    !>routine for the calling of multiple scalars in one single call
    subroutine mpiallred_multi_i0(val1,val2,val3,&
         val4,val5,val6,val7,val8,val9,val10,op,comm,irecvbuf)
      implicit none
      integer(f_integer) :: val1,val2!,val3
      integer(f_integer), optional :: val3,val4,val5,val6,val7,val8,val9,val10
      !local variables
      integer(f_integer), dimension(10) :: tmpsend,tmprecv

      include 'allreduce-multi-inc.f90'
    end subroutine mpiallred_multi_i0
end module f_allreduce