//! @file
//!  OpenCL wrappers
//!
//! @author
//!    Copyright (C) 2009-2011 BigDFT group 
//!    This file is distributed under the terms of the
//!    GNU General Public License, see ~/COPYING file
//!    or http://www.gnu.org/copyleft/gpl.txt .
//!    For the list of contributors, see ~/AUTHORS 

#include <string.h>
#include "OpenCL_wrappers.h"

//void FC_FUNC_(rdtsc,RDTSC)(cl_ulong * t){
//  rdtscll(*t);
//}

 
void FC_FUNC_(ocl_build_programs,OCL_BUILD_PROGRAMS)(bigdft_context * context) {
    liborbs_build_programs(&(*context)->parent);
    build_benchmark_programs(context);
    build_fft_programs(context);
}

void create_kernels(bigdft_context * context, struct bigdft_kernels *kernels){
    create_benchmark_kernels(context, kernels);
    create_fft_kernels(context, kernels);
}



void FC_FUNC_(ocl_create_context,OCL_CREATE_CONTEXT)(bigdft_context * context, const char * platform, const char * devices, cl_int *device_type, cl_uint *device_number) {
    *context = (struct _bigdft_context *)malloc(sizeof(struct _bigdft_context));
    if(*context == NULL) {
      fprintf(stderr,"Error: Failed to create command queue (out of memory)!\n");
      exit(1);
    }
    liborbs_create_context(&(*context)->parent, platform, devices, *device_type, device_number);
    (*context)->fft_size[0] = 0;
    (*context)->fft_size[1] = 0;
    (*context)->fft_size[2] = 0;
}

void FC_FUNC_(ocl_create_gpu_context,OCL_CREATE_GPU_CONTEXT)(bigdft_context * context, cl_uint *device_number) {
    cl_int id = 2;
    FC_FUNC_(ocl_create_context,OCL_CREATE_CONTEXT)(context, "", "", &id, device_number);
}

void FC_FUNC_(ocl_create_cpu_context,OCL_CREATE_CPU_CONTEXT)(bigdft_context * context) {
    cl_int id = 3;
    cl_uint n;
    FC_FUNC_(ocl_create_context,OCL_CREATE_CONTEXT)(context, "", "", &id, &n);
}

void FC_FUNC_(ocl_create_read_buffer_host,OCL_CREATE_READ_BUFFER_HOST)(bigdft_context *context, cl_uint *size, void *host_ptr, cl_mem *buff_ptr ) {
    cl_int ciErrNum = CL_SUCCESS;

    *buff_ptr = clCreateBuffer( (*context)->parent.context, CL_MEM_READ_ONLY | CL_MEM_USE_HOST_PTR, *size, host_ptr, &ciErrNum);

#if DEBUG
    printf("%s %s\n", __func__, __FILE__);
    printf("contexte address: %p, memory address: %p, size: %lu\n",*context,*buff_ptr,(long unsigned)*size);
#endif
    oclErrorCheck(ciErrNum,"Failed to pin buffer!");
}

void FC_FUNC_(ocl_create_write_buffer_host,OCL_CREATE_WRITE_BUFFER_HOST)(bigdft_context *context, cl_uint *size, void *host_ptr, cl_mem *buff_ptr ) {
    cl_int ciErrNum = CL_SUCCESS;

    *buff_ptr = clCreateBuffer( (*context)->parent.context, CL_MEM_WRITE_ONLY | CL_MEM_USE_HOST_PTR, *size, host_ptr, &ciErrNum);

#if DEBUG
    printf("%s %s\n", __func__, __FILE__);
    printf("contexte address: %p, memory address: %p, size: %lu\n",*context,*buff_ptr,(long unsigned)*size);
#endif
    oclErrorCheck(ciErrNum,"Failed to pin buffer!");
}

void FC_FUNC_(ocl_create_read_write_buffer_host,OCL_CREATE_READ_WRITE_BUFFER_HOST)(bigdft_context *context, cl_uint *size, void *host_ptr, cl_mem *buff_ptr ) {
    cl_int ciErrNum = CL_SUCCESS;

    *buff_ptr = clCreateBuffer( (*context)->parent.context, CL_MEM_READ_WRITE | CL_MEM_USE_HOST_PTR, *size, host_ptr, &ciErrNum);

#if DEBUG
    printf("%s %s\n", __func__, __FILE__);
    printf("contexte address: %p, memory address: %p, size: %lu\n",*context,*buff_ptr,(long unsigned)*size);
#endif
    oclErrorCheck(ciErrNum,"Failed to pin buffer!");
}

void FC_FUNC_(ocl_map_read_buffer,OCL_MAP_READ_BUFFER)(bigdft_command_queue *command_queue, cl_mem *buff_ptr, cl_uint *offset, cl_uint *size) {
    cl_int ciErrNum = CL_SUCCESS;
    clEnqueueMapBuffer((*command_queue)->parent.command_queue, *buff_ptr, CL_TRUE, CL_MAP_READ , *offset, *size, 0, NULL, NULL, &ciErrNum);
    oclErrorCheck(ciErrNum,"Failed to map pinned read buffer!");
}

void FC_FUNC_(ocl_map_write_buffer,OCL_MAP_WRITE_BUFFER)(bigdft_command_queue *command_queue, cl_mem *buff_ptr, cl_uint *offset, cl_uint *size) {
    cl_int ciErrNum = CL_SUCCESS;
    clEnqueueMapBuffer((*command_queue)->parent.command_queue, *buff_ptr, CL_TRUE, CL_MAP_WRITE , *offset, *size, 0, NULL, NULL, &ciErrNum);
    oclErrorCheck(ciErrNum,"Failed to map pinned read buffer!");
}

void FC_FUNC_(ocl_map_read_write_buffer,OCL_MAP_READ_WRITE_BUFFER)(bigdft_command_queue *command_queue, cl_mem *buff_ptr, cl_uint *offset, cl_uint *size) {
    cl_int ciErrNum = CL_SUCCESS;
    clEnqueueMapBuffer((*command_queue)->parent.command_queue, *buff_ptr, CL_TRUE, CL_MAP_READ | CL_MAP_WRITE , *offset, *size, 0, NULL, NULL, &ciErrNum);
    oclErrorCheck(ciErrNum,"Failed to map pinned read buffer!");
}

void FC_FUNC_(ocl_map_read_write_buffer_async,OCL_MAP_READ_WRITE_BUFFER_ASYNC)(bigdft_command_queue *command_queue, cl_mem *buff_ptr, cl_uint *offset, cl_uint *size) {
    cl_int ciErrNum = CL_SUCCESS;
    clEnqueueMapBuffer((*command_queue)->parent.command_queue, *buff_ptr, CL_FALSE, CL_MAP_READ | CL_MAP_WRITE , *offset, *size, 0, NULL, NULL, &ciErrNum);
    oclErrorCheck(ciErrNum,"Failed to map pinned read buffer!");
}

void FC_FUNC_(ocl_pin_read_write_buffer,OCL_PIN_READ_WRITE_BUFFER)(bigdft_context *context, bigdft_command_queue *command_queue, cl_uint *size, void *host_ptr, cl_mem *buff_ptr ) {
    cl_int ciErrNum = CL_SUCCESS;

    *buff_ptr = clCreateBuffer( (*context)->parent.context, CL_MEM_READ_WRITE | CL_MEM_USE_HOST_PTR, *size, host_ptr, &ciErrNum);
    
#if DEBUG
    printf("%s %s\n", __func__, __FILE__);
    printf("contexte address: %p, memory address: %p, size: %lu\n",*context,*buff_ptr,(long unsigned)*size);
#endif
    oclErrorCheck(ciErrNum,"Failed to pin read-write buffer!");

    clEnqueueMapBuffer((*command_queue)->parent.command_queue, *buff_ptr, CL_TRUE, CL_MAP_WRITE | CL_MAP_READ , 0, *size, 0, NULL, NULL, &ciErrNum);
    oclErrorCheck(ciErrNum,"Failed to map pinned read-write buffer!");

}

void FC_FUNC_(ocl_pin_write_buffer,OCL_PIN_WRITE_BUFFER)(bigdft_context *context, bigdft_command_queue *command_queue, cl_uint *size, void *host_ptr, cl_mem *buff_ptr ) {
    cl_int ciErrNum = CL_SUCCESS;

    *buff_ptr = clCreateBuffer( (*context)->parent.context, CL_MEM_WRITE_ONLY | CL_MEM_USE_HOST_PTR, *size, host_ptr, &ciErrNum);
    
#if DEBUG
    printf("%s %s\n", __func__, __FILE__);
    printf("contexte address: %p, memory address: %p, size: %lu\n",*context,*buff_ptr,(long unsigned)*size);
#endif
    oclErrorCheck(ciErrNum,"Failed to pin write buffer!");

    clEnqueueMapBuffer((*command_queue)->parent.command_queue, *buff_ptr, CL_TRUE, CL_MAP_READ , 0, *size, 0, NULL, NULL, &ciErrNum);
    oclErrorCheck(ciErrNum,"Failed to map pinned write buffer!");

}

void FC_FUNC_(ocl_pin_read_write_buffer_async,OCL_PIN_READ_WRITE_BUFFER_ASYNC)(bigdft_context *context, bigdft_command_queue *command_queue, cl_uint *size, void *host_ptr, cl_mem *buff_ptr ) {
    cl_int ciErrNum = CL_SUCCESS;

    *buff_ptr = clCreateBuffer( (*context)->parent.context, CL_MEM_READ_WRITE | CL_MEM_USE_HOST_PTR, *size, host_ptr, &ciErrNum);
    
#if DEBUG
    printf("%s %s\n", __func__, __FILE__);
    printf("contexte address: %p, memory address: %p, size: %lu\n",*context,*buff_ptr,(long unsigned)*size);
#endif
    oclErrorCheck(ciErrNum,"Failed to pin read-write buffer!");

    clEnqueueMapBuffer((*command_queue)->parent.command_queue, *buff_ptr, CL_FALSE, CL_MAP_WRITE | CL_MAP_READ , 0, *size, 0, NULL, NULL, &ciErrNum);
    oclErrorCheck(ciErrNum,"Failed to map pinned read-write buffer (async)!");

}

void FC_FUNC_(ocl_pin_write_buffer_async,OCL_PIN_WRITE_BUFFER_ASYNC)(bigdft_context *context, bigdft_command_queue *command_queue, cl_uint *size, void *host_ptr, cl_mem *buff_ptr ) {
    cl_int ciErrNum = CL_SUCCESS;

    *buff_ptr = clCreateBuffer( (*context)->parent.context, CL_MEM_WRITE_ONLY | CL_MEM_USE_HOST_PTR, *size, host_ptr, &ciErrNum);
    
#if DEBUG
    printf("%s %s\n", __func__, __FILE__);
    printf("contexte address: %p, memory address: %p, size: %lu\n",*context,*buff_ptr,(long unsigned)*size);
#endif
    oclErrorCheck(ciErrNum,"Failed to pin write buffer!");

    clEnqueueMapBuffer((*command_queue)->parent.command_queue, *buff_ptr, CL_FALSE, CL_MAP_READ , 0, *size, 0, NULL, NULL, &ciErrNum);
    oclErrorCheck(ciErrNum,"Failed to map pinned write buffer (async)!");

}

void FC_FUNC_(ocl_create_read_buffer_and_copy,OCL_CREATE_READ_BUFFER_AND_COPY)(bigdft_context *context, cl_uint *size, void *host_ptr, cl_mem *buff_ptr) {
    cl_int ciErrNum = CL_SUCCESS;
    *buff_ptr = clCreateBuffer( (*context)->parent.context, CL_MEM_READ_ONLY|CL_MEM_COPY_HOST_PTR, *size, host_ptr, &ciErrNum);
    oclErrorCheck(ciErrNum,"Failed to create initialized read buffer!");
}

void FC_FUNC_(ocl_create_write_buffer,OCL_CREATE_WRITE_BUFFER)(bigdft_context *context, cl_uint *size, cl_mem *buff_ptr) {
    cl_int ciErrNum = CL_SUCCESS;
    *buff_ptr = clCreateBuffer( (*context)->parent.context, CL_MEM_WRITE_ONLY, *size, NULL, &ciErrNum);
#if DEBUG
    printf("%s %s\n", __func__, __FILE__);
    printf("contexte address: %p, memory address: %p, size: %lu\n",*context,*buff_ptr,(long unsigned)*size);
#endif
    oclErrorCheck(ciErrNum,"Failed to create write buffer!");
}

void FC_FUNC_(ocl_create_command_queue,OCL_CREATE_COMMAND_QUEUE)(bigdft_command_queue *command_queue, bigdft_context *context)
{
    *command_queue = (struct _bigdft_command_queue *)malloc(sizeof(struct _bigdft_command_queue));
    if(*command_queue == NULL) {
      fprintf(stderr,"Error: Failed to create command queue (out of memory)!\n");
      exit(1);
    }
    liborbs_create_command_queue(&(*command_queue)->parent, &(*context)->parent, 0);
    (*command_queue)->context = *context;
    create_kernels(context, &(*command_queue)->kernels);
}

void FC_FUNC_(ocl_create_command_queue_id,OCL_CREATE_COMMAND_QUEUE_ID)(bigdft_command_queue *command_queue, bigdft_context *context, cl_uint *index){
    *command_queue = (struct _bigdft_command_queue *)malloc(sizeof(struct _bigdft_command_queue));
    if(*command_queue == NULL) {
      fprintf(stderr,"Error: Failed to create command queue (out of memory)!\n");
      exit(1);
    }
    liborbs_create_command_queue(&(*command_queue)->parent,
                                 &(*context)->parent, *index);
    (*command_queue)->context = *context;
    create_kernels(context, &(*command_queue)->kernels);
}




void FC_FUNC_(ocl_finish,OCL_FINISH)(bigdft_command_queue *command_queue){
    cl_int ciErrNum;
    ciErrNum = clFinish((*command_queue)->parent.command_queue);
    oclErrorCheck(ciErrNum,"Failed to finish!");
}

void FC_FUNC_(ocl_enqueue_barrier,OCL_ENQUEUE_BARRIER)(bigdft_command_queue *command_queue){
    cl_int ciErrNum;
#ifdef CL_VERSION_1_2
    if( compare_opencl_version((*command_queue)->parent.PLATFORM_VERSION, opencl_version_1_2) >= 0 )
      ciErrNum = clEnqueueBarrierWithWaitList((*command_queue)->parent.command_queue, 0, NULL, NULL);
    else
#endif
      ciErrNum = clEnqueueBarrier((*command_queue)->parent.command_queue);
    oclErrorCheck(ciErrNum,"Failed to enqueue barrier!");
}

void FC_FUNC_(ocl_clean_command_queue,OCL_CLEAN_COMMAND_QUEUE)(bigdft_command_queue *command_queue) {
  clean_benchmark_kernels(&((*command_queue)->kernels));
  clean_fft_kernels(&((*command_queue)->context), &((*command_queue)->kernels));
  liborbs_clean_command_queue(&(*command_queue)->parent);
  free(*command_queue);
}

void FC_FUNC_(ocl_clean,OCL_CLEAN)(bigdft_context *context){
  size_t i;
  clean_benchmark_programs(context);
  clean_fft_programs(context);
  for(i=0;i<(*context)->event_number;i++){
    clReleaseEvent((*context)->event_list[i].e);
  }
  liborbs_clean_context(&(*context)->parent);
  free(*context);
}
