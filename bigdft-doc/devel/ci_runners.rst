Continuous integration
======================

BigDFT suite is using Gitlab continuous integration to check that new commits are not breaking existing functions. It is possible to use the Gitlab shared runners (default behaviour in Gitlab) but also to setup one or more custom runners.

Setting-up a dedicated runner
---

Gitlab is providing an extensive documentation on how to setup a private runner, for instance https://docs.gitlab.com/runner/register/

Here is a quick overview on how to do it, using `docker` (commands should be typed by a user that is allowed to interact with docker):

1. Setup the container: ``docker run -d --name gitlab-runner --rm -v gitlab-runner-config:/etc/gitlab-runner -v /var/run/docker.sock:/var/run/docker.sock gitlab/gitlab-runner:latest``. Each time this container is stopped for whatever reason, it should be restarted with the same command when the runner is expected to receive jobs from Gitlab.

2. Setup the new runner: ``docker run -it --rm -v gitlab-runner-config:/etc/gitlab-runner -v /var/run/docker.sock:/var/run/docker.sock gitlab/gitlab-runner:latest register``. Answer the questions with ``https://gitlab.com/``, the token available in the ``settings/ci-cd/runners`` part of your Gitlab project, don't add tags, use ``docker`` executor and ``ubuntu:latest`` as default image.

3. In the ``settings/ci-cd/runners``, uncheck the box allowing to use shared runners and verify that your new dedicated runner is listed in the private runner list and green. Optionally issue: ``docker run -it --rm -v gitlab-runner-config:/etc/gitlab-runner -v /var/run/docker.sock:/var/run/docker.sock gitlab/gitlab-runner:latest verify`` to check that on the work station also.
